## Type change
<!--- What is the MR type? -->
- ( ) Bug fix
- (X) Feature
- ( ) Refactoring

## Description
None

## Dependencies
None

## Trello task / print screen
None

## Related PRB
None

## Time box suggestion
`30` minutes

## Check sonarQube dashboard for this branch
None

## Screenshots (if necessary)
None

## Checklist:
<!--- Check each item that you have done and mark with `X` what had be done -->
- Is need update documentation? ( ) Update the [README.md](README.md).
- [ ] Add test coverage.
- [X] Commits reviewed
- [X] Lint checked
- [X] Tests checked
