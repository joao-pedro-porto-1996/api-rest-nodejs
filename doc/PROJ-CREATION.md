# Project creation

- Create package.json:
```sh
npm init -y
```

- Install `sucrase`, `typescript` and `nodemon`:
```sh
npm install -D sucrase typescript nodemon
```

- Create folder `src` and `server`:
```sh
mkdir src
touch src/server.ts
```

- Install `eslint` for `typescript`:
```sh
npm install -D eslint @typescript-eslint/parser @typescript-eslint/eslint-plugin
npm install -D eslint-config-standard eslint-plugin-import eslint-plugin-node eslint-plugin-promise eslint-plugin-standard
```

- Install `eslint` and start for project:
```sh
npm install eslint --save-dev
npx eslint --init
```

- Install `prettier`:
```sh
npm install -D prettier eslint-config-prettier eslint-plugin-prettier
```

- Install `express`, `cors` and `mongoose`:
```sh
npm install express cors mongoose
npm install -D @types/express @types/cors @types/mongoose
```

- Install `esm`, `bcrypt`, `lodash` and `dotenv`:
```sh
npm install esm bcrypt dotenv lodash
npm install -D @types/bcrypt @types/lodash
```

- Install `chai`, `chai-http`, `mocha`, `mockdate`, `ts-node`, `mongo-unit` and `nyc`:
```sh
npm install -D chai @types/chai chai-http @types/chai-http mocha @types/mocha mockdate ts-node mongo-unit nyc
```

- Install `swagger`
```sh
npm install swagger-ui-express --save
npm install -D @types/swagger-ui-express
```
