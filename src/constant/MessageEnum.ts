enum Message {
  PARAMETER_EMPTLY_OR_NULL = 'There are parameters that were not passed in the request',
  USER_DOESNT_EXIST_OR_DELETED = "Unable to delete user as user doesn't exist or is deleted",
  USER_ACTIVE = 'E-mail already linked to an active user of the system',
  GENERATED_PW_ERROR = 'Could not generate password for user',
  UPDATE_OR_CREATE_USER_ERROR = 'Could not create or update user'
}

export { Message }
