import { expect } from 'chai'

import 'mocha'
import { Message } from '../MessageEnum'

describe('validate messageEnum', () => {
  describe('when PARAMETER_EMPTLY_OR_NULL is specifield', () => {
    it('it should return a message', () => {
      const message = Message.PARAMETER_EMPTLY_OR_NULL
      expect(message).to.be.a('string')

      expect(message)
        .to.deep.equal('There are parameters that were not passed in the request')
    })
  })

  describe('when USER_DOESNT_EXIST_OR_DELETED is specifield', () => {
    it('it should return a message', () => {
      const message = Message.USER_DOESNT_EXIST_OR_DELETED
      expect(message).to.be.a('string')

      expect(message)
        .to.deep.equal("Unable to delete user as user doesn't exist or is deleted")
    })
  })

  describe('when USER_ACTIVE is specifield', () => {
    it('it should return a message', () => {
      const message = Message.USER_ACTIVE
      expect(message).to.be.a('string')

      expect(message)
        .to.deep.equal('E-mail already linked to an active user of the system')
    })
  })

  describe('when GENERATED_PW_ERROR is specifield', () => {
    it('it should return a message', () => {
      const message = Message.GENERATED_PW_ERROR
      expect(message).to.be.a('string')

      expect(message)
        .to.deep.equal('Could not generate password for user')
    })
  })

  describe('when UPDATE_OR_CREATE_USER_ERROR is specifield', () => {
    it('it should return a message', () => {
      const message = Message.UPDATE_OR_CREATE_USER_ERROR
      expect(message).to.be.a('string')

      expect(message)
        .to.deep.equal('Could not create or update user')
    })
  })
})
