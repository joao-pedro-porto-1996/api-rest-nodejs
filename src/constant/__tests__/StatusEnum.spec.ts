import { expect } from 'chai'

import 'mocha'
import { Status } from '../StatusEnum'

describe('validate statusEnum', () => {
  describe('when ACTIVE is specifield', () => {
    it('it should return a string', () => {
      const status = Status.ACTIVE
      expect(status).to.be.a('string')
      expect(status).to.deep.equal('ACTIVE')
    })
  })

  describe('when INACTIVE is specifield', () => {
    it('it should return a string', () => {
      const status = Status.INACTIVE
      expect(status).to.be.a('string')
      expect(status).to.deep.equal('INACTIVE')
    })
  })

  describe('when PENDING is specifield', () => {
    it('it should return a string', () => {
      const status = Status.PENDING
      expect(status).to.be.a('string')
      expect(status).to.deep.equal('PENDING')
    })
  })
})
