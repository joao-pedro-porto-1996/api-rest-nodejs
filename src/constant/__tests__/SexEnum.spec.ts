import { expect } from 'chai'

import 'mocha'
import { Sex } from '../SexEnum'

describe('validate sexEnum', () => {
  describe('when MALE is specifield', () => {
    it('it should return a string', () => {
      const sex = Sex.MALE

      expect(sex).to.be.a('string')
      expect(sex).to.deep.equal('MALE')
    })
  })

  describe('when FEMALE is specifield', () => {
    it('it should return a string', () => {
      const sex = Sex.FEMALE

      expect(sex).to.be.a('string')
      expect(sex).to.deep.equal('FEMALE')
    })
  })
})
