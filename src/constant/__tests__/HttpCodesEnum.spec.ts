import { expect } from 'chai'
import 'mocha'

import { HttpCodes } from '../HttpCodesEnum'

describe('validate httpCodesEnum', () => {
  describe('when SUCCESS is specifield', () => {
    it('it should return code 200', () => {
      expect(HttpCodes.SUCCESS).to.deep.equal(200)
    })
  })

  describe('when BADREQUEST is specifield', () => {
    it('it should return code 400', () => {
      expect(HttpCodes.BAD_REQUEST).to.deep.equal(400)
    })
  })

  describe('when UNAUTHORIZED is specifield', () => {
    it('it should return code 401', () => {
      expect(HttpCodes.UNAUTHORIZED).to.deep.equal(401)
    })
  })

  describe('when NOTFOUND is specifield', () => {
    it('it should return code 404', () => {
      expect(HttpCodes.NOT_FOUND).to.deep.equal(404)
    })
  })

  describe('when APPLICATIONEXCEPTION is specifield', () => {
    it('it should return code 420', () => {
      expect(HttpCodes.APPLICATION_EXCEPTION).to.deep.equal(420)
    })
  })

  describe('when INTERNALSERVERERROR is specifield', () => {
    it('it should return code 500', () => {
      expect(HttpCodes.INTERNAL_SERVER_ERROR).to.deep.equal(500)
    })
  })
})
