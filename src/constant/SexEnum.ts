enum Sex {
  MALE = 'MALE',
  FEMALE = 'FEMALE'
}

export { Sex }
