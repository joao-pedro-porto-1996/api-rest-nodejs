import { Schema, model } from 'mongoose'

import { UserInterface } from '../interfaces/User'

const UserSchema = new Schema({
  _id: Schema.Types.ObjectId,
  id: {
    type: String,
    required: [true, 'Id is required...']
  },
  createdDate: {
    type: Date,
    default: Date.now,
    required: [true, 'Create date is required...']
  },
  editedDate: {
    type: Date,
    default: Date.now
  },
  name: {
    type: String,
    required: [true, 'Name is required...']
  },
  mail: {
    type: String,
    required: [true, 'Mail is required...'],
    unique: true,
    lowercase: true,
    match: /^(([^<>()[\]\\.,:\s@"]+(\.[^<>()[\]\\.,:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  },
  password: {
    type: String,
    required: [true, 'Password is required...'],
    select: false
  },
  status: {
    type: String,
    enum: ['ACTIVE', 'INACTIVE', 'PENDING'],
    required: [true, 'Status is required...']
  }
})

export default model<UserInterface>('User', UserSchema)
