import chai from 'chai'
import mongoUnit from 'mongo-unit'
import chaiHttp from 'chai-http'
import chaiExclude from 'chai-exclude'

import app from '../../server'
import { init } from '../../__tests__/database/mongoDBTest'
import { users } from '../../__tests__/mock/users.json'

chai.use(chaiHttp)
chai.use(chaiExclude)
const { expect } = chai
const PATH = '/api/user'

describe('validate userRoute', () => {
  before(() => init())
  beforeEach(() => {
    mongoUnit.load({ users })
  })
  afterEach(() => {
    mongoUnit.drop()
  })

  describe('POST', () => {
    describe('localhost:3001/api/user', () => {
      describe('when parameters are passed correctly and the email does not exist', () => {
        it('it should return success status and message on body', async () => {
          return chai.request(app)
            .post(PATH)
            .set('Content-Type', 'application/json')
            .send({
              name: 'João Pedro Porto',
              mail: 'joaopedro2100@hotmail.com'
            })
            .then(res => {
              expect(res).to.have.status(200)
              expect(res.body).to.be.a('object')
              const { message } = res.body
              expect(message).to.deep.equal('User successfully added')
            })
            .catch(err => {
              throw err
            })
        })
      })

      describe('when parameters are passed incorrectly', () => {
        it('it should return bad request status and error message on body', async () => {
          return chai.request(app)
            .post(PATH)
            .set('Content-Type', 'application/json')
            .send({
              name: 'Porto'
            })
            .then(res => {
              expect(res).to.have.status(400)
              expect(res.body).to.be.a('object')
              const { error } = res.body
              expect(error).to.deep.equal('There are parameters that were not passed in the request')
            })
            .catch(err => {
              throw err
            })
        })
      })

      describe('when email passed is linked to an active user', () => {
        it('it should return precondition failed status and error message on body', async () => {
          return chai.request(app)
            .post(PATH)
            .set('Content-Type', 'application/json')
            .send({
              name: 'João Pedro Porto',
              mail: 'joao.porto@dextra-sw.com'
            })
            .then(res => {
              expect(res).to.have.status(412)
              expect(res.body).to.be.a('object')
              const { error } = res.body
              expect(error).to.deep.equal('E-mail already linked to an active user of the system')
            })
            .catch(err => {
              throw err
            })
        })
      })

      describe('when email passed is linked to an inactive user', () => {
        it('it should return success status and message on body', async () => {
          return chai.request(app)
            .post(PATH)
            .set('Content-Type', 'application/json')
            .send({
              name: 'João Porto',
              mail: 'joaopedroporto1996@gmail.com'
            })
            .then(res => {
              expect(res).to.have.status(200)
              expect(res.body).to.be.a('object')
              const { message } = res.body
              expect(message).to.deep.equal('User successfully added')
            })
            .catch(err => {
              throw err
            })
        })
      })
    })
  })

  describe('GET', () => {
    describe('localhost:3001/api/user', () => {
      describe('when there are active or pending users in the database', () => {
        it('it should return a list of users and message on body', async () => {
          return chai.request(app)
            .get(PATH)
            .set('Content-Type', 'application/json')
            .then(res => {
              expect(res).to.have.status(200)
              expect(res.body).to.be.a('object')
              const { message, data } = res.body
              expect(message).to.deep.equal('User Listing Successfully Returned')
              expect(data).to.have.length(1)
              expect(data).excludingEvery(['_id']).to.deep.equal([
                {
                  _id: '5e8fb7c26e82ee021d08693c',
                  name: 'João Pedro Porto',
                  mail: 'joao.porto@dextra-sw.com',
                  status: 'PENDING'
                }
              ])
            })
            .catch(err => {
              throw err
            })
        })
      })
    })
  })

  describe('PATCH', () => {
    describe('localhost:3001/api/user/:id', () => {
      describe('when the ID passed belongs to an active or pending user', () => {
        it('it should return success status and message on body', async () => {
          return chai.request(app)
            .patch(`${PATH}/5e8bc9f5c3cf6207a16442ed`)
            .set('Content-Type', 'application/json')
            .send({
              name: 'Porto'
            })
            .then(res => {
              expect(res).to.have.status(200)
              expect(res.body).to.be.a('object')
              const { message } = res.body
              expect(message).to.deep.equal('User updated successfully')
            })
            .catch(err => {
              throw err
            })
        })
      })

      describe('when the user does not exist or is inactive', () => {
        it('it should return precondition failed status and error message on body', async () => {
          return chai.request(app)
            .patch(`${PATH}/5e8c9dc0f2cf420c6e635526`)
            .set('Content-Type', 'application/json')
            .send({
              name: 'Porto'
            })
            .then(res => {
              expect(res).to.have.status(412)
              expect(res.body).to.be.a('object')
              const { error } = res.body
              expect(error).to.deep.equal('Could not create or update user')
            })
            .catch(err => {
              throw err
            })
        })
      })

      describe('when parameters are passed incorrectly', () => {
        it('it should return bad request status and error message on body', async () => {
          return chai.request(app)
            .patch(`${PATH}/5e8c9dc0f2cf420c6e635526`)
            .set('Content-Type', 'application/json')
            .send({})
            .then(res => {
              expect(res).to.have.status(400)
              expect(res.body).to.be.a('object')
              const { error } = res.body
              expect(error).to.deep.equal('There are parameters that were not passed in the request')
            })
            .catch(err => {
              throw err
            })
        })
      })
    })
  })

  describe('DELETE', () => {
    describe('localhost:3001/api/user/:id', () => {
      describe('when the ID passed belongs to an active or pending user', () => {
        it('it should return success status and message on body', async () => {
          return chai.request(app)
            .delete(`${PATH}/5e8bc9f5c3cf6207a16442ed`)
            .set('Content-Type', 'application/json')
            .then(res => {
              expect(res).to.have.status(200)
              expect(res.body).to.be.a('object')
              const { message } = res.body
              expect(message).to.deep.equal('User successfully deleted')
            })
        })
      })

      describe('when the ID passed belongs to an inactive user or does not exist', () => {
        it('it should return precondition failed status and error message on body', async () => {
          return chai.request(app)
            .delete(`${PATH}/5e8c9dc0f2cf420c6e635526`)
            .set('Content-Type', 'application/json')
            .then(res => {
              expect(res).to.have.status(412)
              expect(res.body).to.be.a('object')
              const { error } = res.body
              expect(error).to.deep.equal("Unable to delete user as user doesn't exist or is deleted")
            })
        })
      })
    })
  })
})
