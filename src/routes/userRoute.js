import { Router } from 'express'

import {
  createUser,
  listAllUsers,
  updateUser,
  deleteUser
} from '../controllers/UserController'

const router = Router()

router.get('', listAllUsers)
router.post('', createUser)
router.patch('/:id', updateUser)
router.delete('/:id', deleteUser)

export default router
