import mongoose from 'mongoose'

const options = {
  useNewUrlParser: true,
  useUnifiedTopology: true
}

const init = ():void => {
  mongoose
    .connect(`${process.env.MONGO_TEST_URL}?retryWrites=true&w=majority`, options)
    .then(() => console.log(''))
}

const close = ():void => { mongoose.disconnect() }

export {
  init,
  close
}
