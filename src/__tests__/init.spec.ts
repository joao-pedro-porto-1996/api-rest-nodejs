import mongoUnit from 'mongo-unit'

import { close } from '../__tests__/database/mongoDBTest'

mongoUnit.start().then(() => {
  const url = mongoUnit.getUrl()
  console.log('Test mongo is started: ', url)
  process.env.MONGO_TEST_URL = url
  run()
})

after(() => {
  console.log('Stop Test mongo connection')
  close()
  return mongoUnit.stop()
})
