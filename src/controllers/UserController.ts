import { Request, Response } from 'express'
import mongoose from 'mongoose'
import bcrypt from 'bcrypt'
import _ from 'lodash'

import { Message } from '../constant/MessageEnum'
import { UserInterface } from '../interfaces/User'
import User from '../schemas/User'
import { Status } from '../constant/StatusEnum'
import { createHash } from '../static/ApplicationUtil'
import { HttpCodes } from '../constant/HttpCodesEnum'

const passwordGeneration = async (): Promise<string> => bcrypt
  .hash(createHash(8), 10)
  .then(pw => pw)

const getStatus = (status: Status): { status: string } =>
  ({ status: Status[status] })

const createNewUser = async (name: string, mail: string): Promise<boolean> => {
  const newUser = {
    _id: new mongoose.Types.ObjectId(),
    name,
    mail,
    password: await passwordGeneration(),
    ...getStatus(Status.PENDING)
  }

  return User
    .create({
      ...newUser,
      id: _.get(newUser, '_id')
    })
    .then(user => {
      if (user) return true

      return false
    })
}

const findOneAndDelete = async (id: string): Promise<boolean> => User
  .findOneAndUpdate({
    $and: [{ id }, { status: { $not: /^INACTIVE*/ } }]
  }, {
    $set: {
      ...getStatus(Status.INACTIVE)
    }
  })
  .then(user => {
    if (user) return true

    return false
  })

const getStatusNot = (status: Status): { status: { $not: RegExp } } =>
  status !== Status.PENDING ? ({ status: { $not: /^INACTIVE*/ } }) : null

const findOneAndUpdate = async (
  id: string,
  name: string,
  status: Status,
  password: string
): Promise<boolean> => User
  .findOneAndUpdate({
    $and: [{ id }, { ...getStatusNot(status) }]
  }, {
    $set: {
      ...name && { name },
      ...password && { password },
      ...getStatus(status)
    }
  }, {
    new: true
  })
  .then(user => {
    if (user) return true

    return false
  })

const updateUser = async (req: Request, res: Response): Promise<Response> => {
  const _id = _.get(req, 'params.id')
  const name = _.get(req, 'body.name')

  if (!name) {
    return res.status(HttpCodes.BAD_REQUEST).json({
      error: Message.PARAMETER_EMPTLY_OR_NULL
    })
  }

  const verifyResponse: boolean = await findOneAndUpdate(_id, name, Status.ACTIVE, null)

  const message: string = verifyResponse ? 'User updated successfully' : null
  const error: string = !verifyResponse ? Message.UPDATE_OR_CREATE_USER_ERROR : null
  const statusCode: number = verifyResponse ? HttpCodes.SUCCESS : HttpCodes.PRECONDITION_FAILED

  return res.status(statusCode).json({
    ...message && { message },
    ...error && { error }
  })
}

const listAllUsers = async (req: Request, res: Response): Promise<Response> => User
  .find({ status: { $not: /^INACTIVE*/ } })
  .select({ _id: 1, name: 1, mail: 1, status: 1 })
  .sort({ name: 1 })
  .then(users => res.status(HttpCodes.SUCCESS).json({
    message: 'User Listing Successfully Returned',
    data: users
  }))

const getUserByMail = async (mail: string): Promise<UserInterface> => User
  .findOne({ mail })
  .select({ _id: 1, name: 1, mail: 1, status: 1 })

const createUser = async (req: Request, res: Response): Promise<Response> => {
  const name = _.get(req, 'body.name')
  const mail = _.get(req, 'body.mail')

  if (!name || !mail) {
    return res.status(HttpCodes.BAD_REQUEST).json({
      error: Message.PARAMETER_EMPTLY_OR_NULL
    })
  }

  const user: UserInterface = await getUserByMail(mail)
  const status = _.get(user, 'status')

  if (user && status !== Status.INACTIVE) {
    return res.status(HttpCodes.PRECONDITION_FAILED).json({
      error: Message.USER_ACTIVE
    })
  }

  const verifyResponse: boolean =
    user
      ? await findOneAndUpdate(_.get(user, '_id'), name, Status.PENDING, _.get(passwordGeneration(), 'password'))
      : await createNewUser(name, mail)

  const message: string = verifyResponse ? 'User successfully added' : null
  const error: string = !verifyResponse ? Message.UPDATE_OR_CREATE_USER_ERROR : null
  const statusCode: number = verifyResponse ? HttpCodes.SUCCESS : HttpCodes.PRECONDITION_FAILED

  return res.status(statusCode).json({
    ...message && { message },
    ...error && { error }
  })
}

const deleteUser = async (req: Request, res: Response): Promise<Response> => {
  const _id = _.get(req, 'params.id')

  const verifyResponse: boolean = await findOneAndDelete(_id)

  const message: string = verifyResponse ? 'User successfully deleted' : null
  const error: string = !verifyResponse ? Message.USER_DOESNT_EXIST_OR_DELETED : null
  const statusCode: number = verifyResponse ? HttpCodes.SUCCESS : HttpCodes.PRECONDITION_FAILED

  return res.status(statusCode).json({
    ...message && { message },
    ...error && { error }
  })
}

export {
  createUser,
  listAllUsers,
  updateUser,
  deleteUser
}
