import dotenv from 'dotenv'

import app from './app'

dotenv.config()
const { PORT } = process.env

app.listen(PORT, () => {
  console.log(`Server running localhost: ${PORT}`)
  console.log('To drop server: ctrl + c')
})

export default app
