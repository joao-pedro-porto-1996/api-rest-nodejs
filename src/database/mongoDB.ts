import mongoose from 'mongoose'
import dotenv from 'dotenv'

dotenv.config()

const {
  MONGO_ATLAS_USER,
  MONGO_ATLAS_CLUSTER,
  MONGO_ATLAS_DB,
  MONGO_ATLAS_PW,
  MONGODB_DOCKER,
  NODE_ENV
} = process.env

const connectMongodb = (): void => {
  const url = NODE_ENV === 'PROD' ? `mongodb+srv://${MONGO_ATLAS_USER}:${MONGO_ATLAS_PW}${MONGO_ATLAS_CLUSTER}${MONGO_ATLAS_DB}` : MONGODB_DOCKER
  const options = {
    useCreateIndex: true,
    useFindAndModify: false,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    socketTimeoutMS: 36000,
    keepAlive: true
  }
  mongoose.connect(`${url}?retryWrites=true&w=majority`, options)
    .then(() => console.log('Mongo Connected!'))
    .catch((error) => console.log('Mongo connection error!', error))
}

const disconnectMongodb = (): void => {
  console.log('Mongo disconnect!')
  mongoose.disconnect()
}

export {
  connectMongodb,
  disconnectMongodb
}
