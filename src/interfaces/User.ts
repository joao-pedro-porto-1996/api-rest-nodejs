import { Document } from 'mongoose'

export interface UserInterface extends Document {
  _id: string,
  createdDate: Date,
  editedDate?: Date,
  name: string,
  mail: string,
  password: string
  status: string
}
