import express from 'express'
import cors from 'cors'
import swaggerUi from 'swagger-ui-express'

import * as swaggerDocument from './swagger.json'
import { connectMongodb } from './database/mongoDB'
import userRoute from './routes/userRoute.js'

class App {
    public express: express.Application

    public constructor () {
      this.express = express()
      this.middlewares()
      connectMongodb()
      this.routes()
      this.express.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument))
    }

    private middlewares (): void {
      this.express.use(express.json())
      this.express.use(cors())
    }

    private routes (): void {
      this.express.use('/api/user', userRoute)
    }
}

export default new App().express
