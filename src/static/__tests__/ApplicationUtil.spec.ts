import { expect } from 'chai'

import 'mocha'
import { createHash } from '../ApplicationUtil'

describe('validate createHash', () => {
  describe('when specifying the number of characters', () => {
    it('it should return a string with eight characteres', () => {
      const hash = createHash(8)
      expect(hash).to.be.a('string')
      expect(hash.length).to.deep.equal(8)
    })
  })

  describe('when do not specify the number of characters', () => {
    it('it should return a string with six characteres', () => {
      const hash = createHash(null)
      expect(hash).to.be.a('string')
      expect(hash.length).to.deep.equal(6)
    })
  })
})
