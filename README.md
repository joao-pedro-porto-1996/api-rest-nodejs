# API Rest NodeJs

API Rest typescript with node.js

[![pipeline status](https://gitlab.com/joao-pedro-porto-1996/api-rest-nodejs/badges/master/pipeline.svg)](https://gitlab.com/joao-pedro-porto-1996/api-rest-nodejs/-/jobs)
[![coverage report](https://gitlab.com/joao-pedro-porto-1996/api-rest-nodejs/badges/master/coverage.svg)](https://gitlab.com/joao-pedro-porto-1996/api-rest-nodejs/-/jobs)

## 1st part - Installation
```sh
    # ssh
    git clone git@gitlab.com:joao-pedro-porto-1996/api-rest-nodejs.git

    # https
    git clone https://gitlab.com/joao-pedro-porto-1996/api-rest-nodejs.git
```

<!-- ## Architecture
See below the architecture overview:

    ![APIRestNodeJsArchitecture](doc/images/architecture.jpg) -->

## Prepare environment
- Run script for use docker image mongoDB (always - necessary installed Docker):
```sh
./scripts/dev/mongoDB.sh
```
- Configure connection on Robo 3T (necessary installed Robo 3T):
    ![APIRestNodeJsMongoDB](doc/images/connection-settings.jpg)

- Create ".env" in the project's root directory according to the environment variables

## Environment variables
Variable | Development | Production
--- | --- | ---
PORT | 3001 | 13001
NODE_ENV | DEV | PROD
MONGODB_DOCKER | mongodb://127.0.0.1/mydatabase | -
MONGO_ATLAS_USER | - | Porto
MONGO_ATLAS_PW | - | GAHa8O6R3C8RC7xL
MONGO_ATLAS_CLUSTER | - | @cluster-crud-gmzpx.mongodb.net/
MONGO_ATLAS_DB | - | mydatabase

## 2nd part - Installation
```sh
    # nvm
    nvm use

    # install
    npm install
```

## Usage
```sh
    # start
    npm start

    # test
    npm test

    # coverage
    npm run test:coverage

    # lint
    npm run lint

    # build
    npm run build
```

## Swagger UI

- Run script (localhost):
```sh
./scripts/dev/mongoDB.sh
```

- Run the project (localhost):
```sh
npm start
```

- Access:
```sh
    # localhost
    http://localhost:3001/api-docs/

    # production
```

### Requires
- Node.js and npm
- nvm
- Docker
- git
- Robo 3T (version 1.2)

### Note
For ".editorconfig" and ".env" to work, we need to install the "EditorConfig" and "DotENV" extensions on VS Code.
