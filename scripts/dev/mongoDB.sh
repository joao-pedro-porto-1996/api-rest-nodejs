#!/bin/bash -e

green=`tput setaf 2 bold`
reset=`tput sgr0`
yellow=`tput setaf 3`

{ # try
  docker ps | grep tutum/mongodb > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    docker run -d -p 27017:27017 -p 28017:28017 -e MONGODB_DATABASE="mydatabase" -e AUTH=no tutum/mongodb > /dev/null 2>&1
    docker ps | grep tutum/mongodb
    echo -e "\n${green}Docker image MongoDB running...${reset}\n"
  else
    docker ps | grep tutum/mongodb
    echo -e "\n${yellow}Docker image MongoDB running...${reset}\n"
  fi
} || { # catch
  echo "An unexpected error has occurred..."
}
